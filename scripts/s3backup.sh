#!/bin/bash

# Adapted from cron/templates/s3backup0.erb in the sftphub-puppet code repository

# Mount exports if they're passed in
# Pass in the EFS_MOUNTS variable in the form:
#     'server:/mount1=/target1;server:/mount2=target2;etc...'
if [[ -n "${EFS_MOUNTS}" ]]; then
    OIFS=$IFS
    IFS=';'

    for MOUNT in $EFS_MOUNTS; do
        src_mount=$(echo $MOUNT | cut -d'=' -f1)
        dst_mount=$(echo $MOUNT | cut -d'=' -f2)
        mkdir -p $dst_mount
        mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport $src_mount $dst_mount
    done

    IFS=$OIFS
else
    echo "No mounts passed in, so nothing mounted in the container"
fi

# Trace mode on
#set -x

SOURCE=${BackupSourcePath}
DEST=${BackupS3BucketURI}
# Add '--exclude * --include folder1/* --include folder2/* --delete' to OPTIONS
#OPTIONS='--delete --dryrun'
#OPTIONS='--exclude * --include prd/* --delete'
OPTIONS='--delete'

# Record start time for sync job
echo -en "=============\nStart S3 Sync at $(date)\n=============\n"

## Disable path expansion and wildcard globbing
#set -f
#
## Run our S3 Sync in the background
#aws s3 sync "$SOURCE" "$DEST" ${OPTIONS} 2>&1 >> $LOGFILE &

# Launch concurrent S3 Sync processes using Gnu Parallel
cd "$SOURCE"
parallel --line-buffer -j300% --halt never --joblog /var/log/parallel.log --arg-file /ua-resources/folderlist.txt "aws s3 sync {} $DEST/{} $OPTIONS" &

# Capture sync process ID
PID=$!

## Re-enable path expansion
#set +f

wait "$PID"

# Record end time for sync job
echo -en "=============\nEnd S3 Sync at $(date)\n=============\n"
