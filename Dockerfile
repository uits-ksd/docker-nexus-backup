# For a U of A Nexus Repository Manager Storage Back-Up

# Start with the latest Amazon Linux 2023 tagged image from the AWS public ECR (https://gallery.ecr.aws/amazonlinux/amazonlinux)
FROM public.ecr.aws/amazonlinux/amazonlinux:2023
LABEL maintainer="U of A Kuali DevOps <katt-support@list.arizona.edu>"

# Install parallel, unzip, and AWS EFS utilities, then clean cache
RUN yum -y install parallel unzip amazon-efs-utils && \
    yum clean all

# Install AWS CLI Version 2 (see https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

# Copy in scripts
RUN mkdir -p /ua-scripts
RUN chown root:root /ua-scripts
COPY scripts ua-scripts

# Make backup script executable
RUN chmod +x ua-scripts/s3backup.sh && \
    chown root:root ua-scripts/s3backup.sh

# Copy in folder list
RUN mkdir -p /ua-resources
RUN chown root:root /ua-resources
COPY resources ua-resources
    
ENTRYPOINT ua-scripts/s3backup.sh