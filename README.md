# University of Arizona Kuali Nexus Storage Backup Docker Container

---

This repository is for the Kuali team's backup of the Nexus repository storage in AWS.

### Description
This project defines an image that will back-up the Nexus Repository storage to S3 via a running Docker container.

### Requirements
This is based on the **amazonlinux:2023** tagged image from https://gallery.ecr.aws/amazonlinux/amazonlinux.

Our Nexus repository requires persistent storage that needs to be regularly backed up. We chose to back-up the contents of the EFS volume to a **kfs-nexus-repo-storage-backup** S3 bucket in our non-production account. 

### Building
#### Local Testing
A sample build command is: `docker build -t kuali/nexus-storage-backup:local --force-rm .`

This command will build a **kuali/nexus-storage-backup** image and tag it with the name _local_.

#### AWS
Utilizing tags is a way for us to build specific images for an environment. Following semantic versioning, we will tag our images like _ua-release-1.0_ when they are built via a Jenkins job. We will also append the date the image was created to additionally help us version our tags. Example: _ua-release-1.0-2018-08-03_.

### Running
#### Local Testing
A sample command to run a container called nexus-storage-backup on your laptop: `docker run -d --name nexus-storage-backup -v /Users/hlo/docker-nexus-data:/sonatype-work kuali/nexus-storage-backup:local`. For any meaningful testing (that does not require AWS resources), you should make sure the local directory exists. You may also need to temporarily add lines like the following so the container does not exit:
```
touch /var/log/testing.log && chmod a+w /var/log/testing.log
tail -f /var/log/testing.log
```

#### AWS
For AWS we are using ECS. We are also mounting an EFS volume for the "sonatype-work" directories when the container is started. This backup container runs on the existing Nexus ECS cluster as a scheduled task.

### Additional Technical Details
The EFS volume for the "sonatype-work" directories is the same volume used by the Nexus Repository Manager, running in a Docker container.
